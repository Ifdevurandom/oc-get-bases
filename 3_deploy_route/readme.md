```bash
$ oc get routes
NAME      HOST/PORT    PATH   SERVICES     PORT   TERMINATION   WILDCARD
$ oc apply -f deploy_route.yaml
route.route.openshift.io/webroute created
$ oc get routes
#### editato per renderlo più leggibile
NAME: webroute
HOST/PORT: webroute-lcontessa-dev.apps.sandbox.x8i5.p1.openshiftapps.com
PATH:
SERVICE: webserver
PORT: <all>
TERMINATION:
WILDCARD: none
```
