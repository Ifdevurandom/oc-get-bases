# Deploy service 
### con IP di cluster
```bash
$ oc get service

NAME                                      TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
workspace<ID>-service                     ClusterIP   172.30.160.221   <none>        4444/TCP   26d

$ oc apply -f deploy_service.yaml
service/web-service created
$ oc get services
NAME                                      TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)    AGE
web-service                               ClusterIP   172.30.136.123   <none>        80/TCP     4m19s
workspace<ID>-service                     ClusterIP   172.30.160.221   <none>        4444/TCP   26d
```

### con IP esterno
Per ottenere un IP esterno nelle impostazioni interne di openshift dovremo mettere un range di IP che potremo utilizzare in /etc/origin/master/master-config.yaml
```yaml
networkConfig:
  externalIPNetworkCIDRs:
  - <ip_address>/<cidr>
```
Dopodiché si deploya il servizio come in precedenza e nella colonna external-ip apparirà 