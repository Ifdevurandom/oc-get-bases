```bash
$ oc create secret generic my-secret --from-literal=key1=supersecret --from-literal=key2=topsecret
$ oc describe secret/my-secret
Name:         my-secret
Namespace:    lcontessa-dev
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
key1:  11 bytes
key2:  9 bytes
```