```bash
$ oc get pods
No resources found in <namespace> namespace.
$ oc deploy -f deploy_pod.yaml
pod/new-webserver created
$ oc get pods
NAME                                                       READY   STATUS    RESTARTS   AGE
new-webserver                                              1/1     Running   0          59s
$ oc describe pod/new-webserver
Name:                 new-webserver
Namespace:            lcontessa-dev
Priority:             -3
Priority Class Name:  sandbox-users-pods
Node:                 ip-10-0-171-42.ec2.internal/10.0.171.42
Start Time:           Tue, 11 Jan 2022 12:59:42 +0100
Labels:               name=apacheWebserver
Annotations:          k8s.v1.cni.cncf.io/network-status:
                        [{
                            "name": "openshift-sdn",
                            "interface": "eth0",
                            "ips": [
                                "10.131.3.201"
                            ],
                            "default": true,
                            "dns": {}
                        }]
                      k8s.v1.cni.cncf.io/networks-status:
                        [{
                            "name": "openshift-sdn",
                            "interface": "eth0",
                            "ips": [
                                "10.131.3.201"
                            ],
                            "default": true,
                            "dns": {}
                        }]
                      openshift.io/scc: restricted
Status:               Running
IP:                   10.131.3.201
IPs:
  IP:  10.131.3.201
Containers:
  webserver:
    Container ID:   cri-o://a2bde526e1dd3aaeec4d1a29c656e7ea53e4292bc269827fd5444f0f061bd37b
    Image:          image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest
    Image ID:       image-registry.openshift-image-registry.svc:5000/openshift/httpd@sha256:62cfaa2a199641010aab4c4f3fba24cb4040729dd31420438380a4b791f69861
    Port:           8080/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Tue, 11 Jan 2022 12:59:47 +0100
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     500m
      memory:  256Mi
    Requests:
      cpu:        500m
      memory:     256Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-wbhg9 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-wbhg9:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
    ConfigMapName:           openshift-service-ca.crt
    ConfigMapOptional:       <nil>
QoS Class:                   Guaranteed
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/memory-pressure:NoSchedule op=Exists
                             node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason          Age   From               Message
  ----    ------          ----  ----               -------
  Normal  Scheduled       38s   default-scheduler  Successfully assigned lcontessa-dev/new-webserver to ip-10-0-171-42.ec2.internal
  Normal  AddedInterface  34s   multus             Add eth0 [10.131.3.201/23] from openshift-sdn
  Normal  Pulling         33s   kubelet            Pulling image "image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest"
  Normal  Pulled          33s   kubelet            Successfully pulled image "image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest" in 160.99777ms
  Normal  Created         33s   kubelet            Created container webserver
  Normal  Started         33s   kubelet            Started container webserver
```